DOC = doc/html

INSTALL = install -d -m 644
PREFIX= $(DESTDIR)/usr

all:

install: $(DOC)
	install -d $(PREFIX)/bin
	install -d $(PREFIX)/share/man/man1
	install -d $(PREFIX)/share/doc/2vcard
	install -c -m 755 src/2vcard $(PREFIX)/bin
	install -c -m 644 doc/2vcard.1 $(PREFIX)/share/man/man1
	$(INSTALL) $(DOC) $(PREFIX)/share/doc/2vcard
